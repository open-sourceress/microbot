#![no_std]
#![no_main]

use alt_stm32f30x_hal::{
    gpio::PushPull,
    prelude::*,
    stm32::{CorePeripherals, Peripherals, SYST},
    timer::{tim2::Timer as Timer2, tim3::Timer as Timer3, Event},
};
use core::{
    cell::RefCell,
    sync::atomic::{AtomicU32, Ordering},
};
use cortex_m::interrupt::{self, Mutex};
use cortex_m_rt::{entry, exception};
#[cfg(feature = "semihosting")]
use cortex_m_semihosting::hprintln;
use l3gd20::{self, L3gd20, Odr as GyroOdr, Scale};
use lsm303dlhc::{AccelOdr, Lsm303dlhc, MagOdr, Sensitivity};
use micromath::F32Ext as _;
use nb::Error;
#[cfg(not(feature = "semihosting"))]
use panic_halt as _;
#[cfg(feature = "semihosting")]
use panic_semihosting as _;
use void::unreachable;

// f3::leds::Leds::new takes ownership of all of the GPIOE block, but we need parts of GPIOE for other things. Thus we
// create our own LEDs abstraction.
mod leds;
use leds::{Led, Leds};

/// Ad-hoc oneshot channel to transfer ownership of the `Leds` from the entry point to the systick handler.
///
/// Once the entry point has initialized the LEDs, it moves them here. The first time the SysTick handler runs, it takes
/// them from here and moves them into a local static for later use.
static LEDS_CHANNEL: Mutex<RefCell<Option<Leds>>> = Mutex::new(RefCell::new(None));

/// Ad-hoc watcher channel to feed yaw information from the gyro to the systick handler.
///
/// Values stored here are `f32`s reinterpreted as `u32`s in native endianness. This is to work around the fact that
/// `AtomicF32` doesn't exist.
static YAW: AtomicU32 = AtomicU32::new(0);

#[entry]
fn main() -> ! {
    let core_periph = CorePeripherals::take().unwrap(); // Peripherals on all Cortex-M devices
    let device_periph = Peripherals::take().unwrap(); // Peripherals specific to stm32f303 processor
    let mut flash = device_periph.FLASH.constrain();
    let mut rcc = device_periph.RCC.constrain(); // Reset and Clock Control
    let gpioa = device_periph.GPIOA.split(&mut rcc.ahb);
    let gpiob = device_periph.GPIOB.split(&mut rcc.ahb);
    let gpioe = device_periph.GPIOE.split(&mut rcc.ahb);

    let clock_speeds = rcc.cfgr.sysclk(64.mhz()).pclk1(32.mhz()).freeze(&mut flash.acr);

    let leds = Leds::new(
        gpioe.pe9, gpioe.pe10, gpioe.pe11, gpioe.pe12, gpioe.pe13, gpioe.pe14, gpioe.pe15, gpioe.pe8,
    );
    interrupt::free(|cs| *LEDS_CHANNEL.borrow(&cs).borrow_mut() = Some(leds));

    // SPI
    let spi = device_periph
        .SPI1
        .spi((gpioa.pa5, gpioa.pa6, gpioa.pa7), l3gd20::MODE, 1.mhz(), clock_speeds);

    // Gyro
    let mut cs = gpioe.pe3.output().output_type(PushPull);
    cs.set_high().unwrap();
    let mut gyro = L3gd20::new(spi, cs).unwrap();
    assert_eq!(gyro.who_am_i().unwrap(), 0xD4);
    // const DT: f32 = 1.0 / 760.0;
    gyro.set_odr(GyroOdr::Hz760).unwrap();
    const SCALE: Scale = Scale::Dps250;
    gyro.set_scale(SCALE).unwrap();

    // I2C
    let i2c = device_periph.I2C1.i2c((gpiob.pb6, gpiob.pb7), 400.khz(), clock_speeds);

    // Accelerometer/magnetometer
    let mut accelerometer = Lsm303dlhc::new(i2c).unwrap();
    accelerometer.accel_odr(AccelOdr::Hz400).unwrap();
    accelerometer.mag_odr(MagOdr::Hz220).unwrap();
    // const ACCEL_SCALE: f32 = 1.0 / (1 << 14) as f32; // G1 gives +/-2G, reading is an i16, so 1G is 0b00100000_00000000
    accelerometer.set_accel_sensitivity(Sensitivity::G1).unwrap();

    // Timer to wait for new data from the accelerometer
    let mut accel_timer = Timer2::new(device_periph.TIM2, 400.hz(), clock_speeds);
    accel_timer.listen(Event::TimeOut);
    accel_timer.enable();

    // Timer to wait for new data from the magnetometer
    let mut mag_timer = Timer3::new(device_periph.TIM3, 200.hz(), clock_speeds);
    mag_timer.listen(Event::TimeOut);
    mag_timer.enable();

    // SysTick interrupt
    let mut tick = core_periph.SYST;
    tick.set_reload(SYST::get_ticks_per_10ms() * 100); // 0.1s
    tick.clear_current();
    tick.enable_counter();
    tick.enable_interrupt();

    // let mut yaw = 0f32;
    loop {
        // if gyro.status().unwrap().z_new {
        //     let readings = gyro.gyro().unwrap();
        //     yaw += SCALE.degrees(readings.z) * DT;
        //     YAW.store((yaw + 180.0).to_bits(), Ordering::SeqCst); // South LED is a better spot for 0deg, so add 180deg
        // }

        // match accel_timer.wait() {
        //     Ok(()) => {
        //         let readings = match accelerometer.accel() {
        //             Ok(x) => x,
        //             #[cfg_attr(not(feature = "semihosting"), allow(unused))]
        //             Err(e) => {
        //                 #[cfg(feature = "semihosting")]
        //                 hprintln!("error reading accelerometer: {:?}", e).unwrap();
        //                 continue;
        //             }
        //         };
        //         let x = readings.x as f32 * ACCEL_SCALE;
        //         let y = readings.y as f32 * ACCEL_SCALE;
        //         let (x, y) = (y, -x); // Translate accel's frame (+x south, +y east) to our frame (+x east, +y north)

        //         let angle = y.atan2_norm(x); // returns 0 for 0rad, 1 for PI/2 rad, 2 for PI rad, ..
        //         let yaw = (1.0 - angle) * 90.0; // Translate from "0 is right, + is CCW" to "0 is up, + is CW"
        //         YAW.store(yaw.to_bits(), Ordering::SeqCst);
        //     }
        //     Err(Error::Other(void)) => unreachable(void),
        //     Err(Error::WouldBlock) => (),
        // }

        // TODO calibgrate magnetometer
        match mag_timer.wait() {
            Ok(()) => {
                let readings = match accelerometer.mag() {
                    Ok(x) => x,
                    #[cfg_attr(not(feature = "semihosting"), allow(unused))]
                    Err(e) => {
                        #[cfg(feature = "semihosting")]
                        hprintln!("error reading magnetometer: {:?}", e).unwrap();
                        continue;
                    }
                };
                let x = readings.x as f32;
                let y = readings.y as f32;
                let (x, y) = (y, -x); // Translate mag's frame (+x south, +y east) to our frame (+x east, +y north)

                let angle = y.atan2_norm(x);
                let yaw = (1.0 - angle) * 90.0; // Translate from "0 is right, + is CCW" to "0 is up, + is CW"
                YAW.store(yaw.to_bits(), Ordering::SeqCst);
            }
            Err(Error::Other(void)) => unreachable(void),
            Err(Error::WouldBlock) => (),
        }
    }
}

#[exception]
fn SysTick() {
    // Local storage for the LEDs. Once we get access to them from `LEDS_CHANNEL`, move them to a local static to avoid
    // unnecessary synchronization. (Note: cortex_m makes `static mut` in interrupt handlers safe)
    static mut LOCAL_LEDS: Option<[Led<()>; 8]> = None;

    if LOCAL_LEDS.is_none() {
        *LOCAL_LEDS = interrupt::free(|cs| LEDS_CHANNEL.borrow(&cs).borrow_mut().take()).map(Leds::downgrade);
    }

    if let Some(leds) = LOCAL_LEDS.as_mut() {
        // Theoretically this is always true, because the systick interrupt is enabled after the LEDs are already
        // initialized. However, we'll still use if-let instead of unwrap() because if that's wrong, it's not worth
        // panicking over.

        let yaw = f32::from_bits(YAW.load(Ordering::SeqCst));
        let yaw = (yaw % 360.0 + 360.0) % 360.0; // constrain to 0..360
        let ind = (yaw + 22.5) as usize / 45; // cut compass into 8 slices, where 0 is centered on 0deg
        leds.iter_mut().for_each(|led| led.off());
        leds[ind % 8].on();
    }
}
