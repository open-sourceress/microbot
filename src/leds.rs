#![allow(unused)]
use alt_stm32f30x_hal::gpio::{self, LowSpeed, Output, OutputType, PinMode, PullNone, PullType, PushPull};
use core::fmt::{self, Debug, Formatter};
use embedded_hal::digital::v2::OutputPin;

/// Newtype wrapper around a pin, representing one of the eight user-controllable LEDs in the ring.
///
/// The type parameter encodes which LED this struct represents. See the `LedKind` trait for more details.
pub struct Led<K: LedKind> {
    /// The wrapped pin
    pin: K::Pin,
}

impl<K: LedKind> Led<K> {
    /// Turn the LED on.
    pub fn on(&mut self) {
        // OutputPin impls for PE8-PE15 cannot fail, but use () as the error type
        match self.pin.set_high() {
            Ok(()) => (),
            Err(()) => unreachable!("error turning LED on"),
        }
    }

    /// Turn the LED off.
    pub fn off(&mut self) {
        match self.pin.set_low() {
            Ok(()) => (),
            Err(()) => unreachable!("error turning LED off"),
        }
    }

    /// Erase the direction information from this LED's type.
    ///
    /// This is useful for storing multiple LEDs in an array, where they all must have the same type.
    pub fn downgrade(self) -> Led<()> {
        Led {
            pin: K::downgrade(self.pin),
        }
    }

    /// Destroy this wrapper, releasing ownership of its pin to the caller.
    pub fn free(self) -> K::Pin {
        self.pin
    }
}

/// A trait for type-states that represent a particular LED.
///
/// This trait has nine implementors: `North`/`Northeast`/etc, and `()`. `()` represents a type-erased LED.
pub trait LedKind {
    /// The pin type
    type Pin: OutputPin<Error = ()>;

    /// Erase the pin number from the associated pin type.
    fn downgrade(pin: Self::Pin) -> gpio::PEx<PullNone, Output<PushPull, LowSpeed>>;
}

impl LedKind for () {
    type Pin = gpio::PEx<PullNone, Output<PushPull, LowSpeed>>;

    fn downgrade(pin: Self::Pin) -> Self::Pin {
        pin
    }
}

impl Debug for Led<()> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        f.debug_struct("Led<()>").field("pin", &"gpio::PEx (erased)").finish()
    }
}

macro_rules! doc {
    ($(#[$attr:meta])* [$($doc_line:expr)+] $($body:tt)+) => {
        $(#[$attr])*
        $(#[doc = $doc_line])*
        $($body)+
    };
}

macro_rules! declare_types {
    ($($name_snake:ident, $name_title:ident, $abbr_caps:ident = ($ind:literal, $pin:ident, $pull_type:ident)),* $(,)?) => {
        $(
            doc! {
                #[derive(Copy, Clone, Debug)]
                [
                    concat!("Type-state representing the ", stringify!($name_snake), " LED.")
                    ""
                    "This type is only useful in conjunction with the `Led` struct. See that type for more details."
                ]
                pub struct $name_title;
            }

            impl LedKind for $name_title {
                type Pin = gpio::$pin<PullNone, Output<PushPull, LowSpeed>>;

                fn downgrade(pin: Self::Pin) -> gpio::PEx<PullNone, Output<PushPull, LowSpeed>> {
                    pin.downgrade()
                }
            }

            impl Led<$name_title> {
                doc! {
                    [concat!("Create an `Led` representing the ", stringify!($name_snake), " LED.")]
                    pub fn $name_snake(pin: gpio::$pin<PullNone, Output<PushPull, LowSpeed>>) -> Self {
                        Self { pin }
                    }
                }
            }

            impl Debug for Led<$name_title> {
                fn fmt(&self, f: &mut Formatter) -> fmt::Result {
                    f.debug_struct(concat!("Led<", stringify!($name_title), ">"))
                        .field("pin", &concat!("gpio::", stringify!($pin)))
                        .finish()
                }
            }
        )*

        /// A container for all 8 LEDs.
        ///
        /// If you have created instances of `Led` already, including converting the pins to the correct mode, you can
        /// create this wrapper by normal struct initialization:
        ///
        /// ```ignore
        /// let gpioe = // ..
        /// let north = Led::north(gpioe.pe9.into_push_pull_output(&mut gpioe.moder, &mut gpioe.otyper));
        /// let northeast = Led::northeast(gpioe.pe10.into_push_pull_output(&mut gpioe.moder, &mut gpioe.otyper));
        /// // ..
        /// let leds = Leds {
        ///     north,
        ///     northeast,
        ///     // ..
        /// }
        /// ```
        ///
        /// To reduce the boilerplate required for setting all 8 pins to the correct mode and wrapping them in their
        /// respective newtypes, see the `new` method.
        #[derive(Debug)]
        pub struct Leds {
            $(pub $name_snake: Led<$name_title>,)*
        }

        impl Leds {
            /// Construct all the LEDs from bare pins.
            ///
            /// This method converts each pin to the correct mode, wraps it in the correct newtype wrapper, and collects
            /// all newtypes into this type for you. This greatly reduces the amount of code required when initializing
            /// the LEDs:
            ///
            /// ```ignore
            /// let gpioe = /* .. */;
            /// let leds = Leds::new(gpioe.pe9, gpioe.pe10, /* ..all 8 pins.. */);
            /// ```
            ///
            /// Note: the pin parameter types are the specific pins matching to each LED. Passing a different pin for
            /// any parameter produces a compilation error.
            // macro expansion: `pub fn new<N, PN, NE, PNE, ..>(north: PE9<PN, N>, northeast: PE10<PNE, NE>, ..) -> Self`
            #[inline]
            pub fn new<$($abbr_caps: PinMode, $pull_type: PullType),*>(
                $($name_snake: gpio::$pin<$pull_type, $abbr_caps>,)*
            ) -> Self {
                Self {
                    // macro expansion: `north: Led::north(north.pull_type(PullNone).output()), ..`
                    $($name_snake: Led::$name_snake($name_snake.pull_type(PullNone).output())),*
                }
            }

            /// Erase the direction from all LEDs' types, and return the erased LEDs in an array.
            ///
            /// This is useful for iteration over LEDs as a homogenous collection.
            ///
            /// The order of the returned LEDs starts at north and proceeds clockwise, e.g. N, NE, E, SE, .. This means
            /// that `led_array[Direction::Southeast as usize]` is the southeast LED, for example.
            #[inline]
            pub fn downgrade(self) -> [Led<()>; 8] {
                [
                    $(self.$name_snake.downgrade()),*
                ]
            }
        }

        /// A direction an LED points.
        #[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
        pub enum Direction {
            $($name_title = $ind,)*
        }

        impl Direction {
            /// Get the direction representing the given index, or `None` if out of range.
            ///
            /// This is approximately the inverse of `some_direction as usize`. Namely, for any `d: Direction`,
            /// `Direction::from_index(d as usize) == Some(d)`.
            pub fn from_index(ind: usize) -> Option<Self> {
                match ind {
                    $($ind => Some(Self::$name_title),)*
                    _ => None,
                }
            }
        }
    };
}

impl Direction {
    /// Return the direction directly clockwise from this one.
    ///
    /// This is the inverse of `next_counterclockwise`.
    #[inline]
    pub fn next_clockwise(self) -> Self {
        let ind = self as usize + 1;
        match Self::from_index(ind % 8) {
            Some(dir) => dir,
            None => unreachable!("Direction::next_clockwise returned None"),
        }
    }

    /// Return the direction directly counter-clockwise from this one.
    ///
    /// This is the inverse of `next_clockwise`.
    #[inline]
    pub fn next_counterclockwise(self) -> Self {
        let ind = self as usize + 7; // + 7 instead of - 1 so we don't interact with negative numbers
        match Self::from_index(ind % 8) {
            Some(dir) => dir,
            None => unreachable!("Direction::next_counterclockwise returned None"),
        }
    }
}

declare_types! {
    north, North, N = (0, PE9, PN),
    northeast, Northeast, NE = (1, PE10, PNE),
    east, East, E = (2, PE11, PE),
    southeast, Southeast, SE = (3, PE12, PSE),
    south, South, S = (4, PE13, PS),
    southwest, Southwest, SW = (5, PE14, PSW),
    west, West, W = (6, PE15, PW),
    northwest, Northwest, NW = (7, PE8, PNW),
}
